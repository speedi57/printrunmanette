#!/usr/bin/python
# -*- coding: utf-8 -*-
 
# source d'inspiration: http://wiki.wxpython.org/cx_freeze
 
import sys, os
from cx_Freeze import setup, Executable
 
#############################################################################
# preparation des options 
path = sys.path
includes = []
excludes = ['_ssl', 'calendar', 'Tkconstants', 'Tkinter', 'tcl', 'email']
packages = ["sys","os","time", "pygame", "configparser","numpy","wx","printrun"]
includefiles = ["./images/","./locale/","pronterface.png","pronterface.appdata.xml","skeinforge"]


options = {"path": path,
           "includes": includes,
		   "include_files": includefiles,
           "excludes": excludes,
           "packages": packages
           }
 
#############################################################################
# preparation des cibles
base = None
if sys.platform == "win32":
    base = "Win32GUI"
 
cible_1 = Executable(
    script = "pronterface.py",
    base = base,
    compress = True,
    icon = "./pronterface.ico",
    )
 
#############################################################################
# creation du setup
setup(
    name = "Pronterface",
    version = "0.1",
    description = "Pronterface for Graoulab",
    author = "speedi57",
    options = {"build_exe": options},
    executables = [cible_1]
    )
